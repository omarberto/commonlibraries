﻿using OpenSSL.PrivateKeyDecoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace OpenSSL
{
    /// <summary>
    /// CertificateFromFileProvider
    /// </summary>
    public class CertificateFromFileProvider : BaseCertificateProvider//, ICertificateProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CertificateFromFileProvider"/> class.
        /// </summary>
        /// <param name="certificateText">The certificate or public key text.</param>
        /// <param name="privateKeyText">The private (rsa) key text.</param>
        /// <param name="securePassword">The optional securePassword to decrypt the private key.</param>
        public CertificateFromFileProvider(string certificateText, string privateKeyText, SecureString securePassword) :
            this(certificateText, privateKeyText, false, securePassword)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificateFromFileProvider"/> class.
        /// </summary>
        /// <param name="certificateText">The certificate or public key text.</param>
        /// <param name="privateKeyText">The private (rsa) key text.</param>
        /// <param name="useKeyContainer">Store the private key in a key container. <see href="https://docs.microsoft.com/en-us/dotnet/standard/security/how-to-store-asymmetric-keys-in-a-key-container" /></param>
        /// <param name="securePassword">The optional securePassword to decrypt the private key.</param>
        /// <exception cref="ArgumentNullException">certificateText or privateKeyText</exception>
        public CertificateFromFileProvider(string certificateText, string privateKeyText, bool useKeyContainer = false, SecureString securePassword = null)
        {
            //RSACryptoServiceProvider.UseMachineKeyStore = true;
            if (certificateText == null)
            {
                throw new ArgumentNullException(nameof(certificateText));
            }

            if (privateKeyText == null)
            {
                throw new ArgumentNullException(nameof(privateKeyText));
            }

            Certificate = new X509Certificate2(GetPublicKeyBytes(certificateText));
#if NETSTANDARD
            PublicKey = Certificate.GetRSAPublicKey();
#else
            PublicKey = (RSACryptoServiceProvider)Certificate.PublicKey.Key;
#endif

            IOpenSSLPrivateKeyDecoder decoder = new OpenSSLPrivateKeyDecoder();
            var privateKey = decoder.Decode(privateKeyText, securePassword);

            if (useKeyContainer)
            {
                var cspParameters = new CspParameters { KeyContainerName = $"{{{Guid.NewGuid()}}}" };
                var cspPrivateKey = new RSACryptoServiceProvider(cspParameters);
                cspPrivateKey.ImportParameters(privateKey.ExportParameters(true));
                PrivateKey = cspPrivateKey;
            }
            else
            {
                var cspParameters = new CspParameters { KeyContainerName = $"{{{Guid.NewGuid()}}}" };

                cspParameters.Flags = CspProviderFlags.UseMachineKeyStore;//UseNonExportableKey -- UseMachineKeyStore
                Console.WriteLine("TEST arci!! {0} -- {2} -- {1}", cspParameters.Flags, cspParameters.KeyContainerName, cspParameters.ProviderName);
                //RSACryptoServiceProvider.UseMachineKeyStore = true;
                var cspPrivateKey = new RSACryptoServiceProvider(cspParameters);
                cspPrivateKey.ImportParameters(privateKey.ExportParameters(true));
                PrivateKey = cspPrivateKey;
            }

#if !NETSTANDARD
            Certificate.PrivateKey = PrivateKey;
            Console.WriteLine("MEIO");
#endif
        }

        /// <summary>
        /// Gets the generated X509Certificate2 object.
        /// </summary>
        public X509Certificate2 Certificate { get; }

        /// <summary>
        /// Gets the PrivateKey object.
        /// </summary>
        public RSACryptoServiceProvider PrivateKey { get; }

        /// <summary>
        /// Gets the PublicKey object.
        /// </summary>
#if NETSTANDARD
        public RSA PublicKey { get; }
#else
        public RSACryptoServiceProvider PublicKey { get; }
#endif
    }
}
